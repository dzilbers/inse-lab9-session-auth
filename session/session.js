let express = require('express');
let session = require('express-session');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let debug = require('debug')('lab7:session');
let app = express();

let secret = 'lab7 session';
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.use(logger('dev'));
app.use(cookieParser(secret));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(session({
    secret: secret,
    resave: false,
    saveUninitialized: false
}));

app.all('/*', async (req, res, next) => {
    debug('headers');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With');
    next();
});

let sess;

app.all('/*', async (req, res, next) => {
    sess = req.session;
    debug(sess);
    next();
});

app.get('/', async (req,res) => {
    //Session set when user Request our app via URL
    if (sess.email) {
        /*
         * This line check Session existence.
         * If it existed will do some action.
         */
        debug('redirect to /admin');
        res.redirect('/admin');
    } else {
        debug('render index.html');
        res.render('index.html');
    }
});

app.post('/login', async (req, res) => {
    //In this we are assigning email to sess.email variable.
    //email comes from HTML page.
    sess.email = req.body.email;
    debug(sess);
    res.end('done');
});

app.get('/admin', async (req,res) => {
    if(sess.email) {
        res.write('<h1>Hello ' + sess.email + '</h1>');
        res.end('<a href="/logout">Logout</a>');
    } else {
        res.write('<h1>Please login first.</h1>');
        res.end('<a href="/">Login</a>');
    }
});

app.get('/logout', async (req,res) => {
    req.session.destroy(err => {
        if(err) {
            console.log(err);
        } else {
            res.redirect('/');
        }
    });
});

app.listen(8080, () => {
    debug("App Started on PORT 8080");
});
